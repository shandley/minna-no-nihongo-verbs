"use strict";
exports.__esModule = true;
var verbs = [
    {
        "id": "bUCZylqhh",
        "chapter": 4,
        "english": "wake",
        "kanji": "起きます",
        "verb_group": 1,
        "polite_form": {
            "present": "おきます",
            "present_negative": "おきません",
            "past": "おきました",
            "past_negative": "おきませんでした"
        },
        "plain_form": {
            "present": "おく",
            "present_negative": "おかない",
            "past": "おいた",
            "past_negative": "おかなかった"
        },
        "te_form": {
            "present": "おきて"
        }
    },
    {
        "id": "9JcNZNMlgd",
        "chapter": 5,
        "english": "go (location)",
        "kanji": "行きます",
        "verb_group": 1,
        "polite_form": {
            "present": "いきます",
            "present_negative": "いきません",
            "past": "いきました",
            "past_negative": "いきませんでした"
        },
        "plain_form": {
            "present": "いく",
            "present_negative": "いかない",
            "past": "いいた",
            "past_negative": "いかなかった"
        },
        "te_form": {
            "present": "いって"
        }
    },
    {
        "id": "fPbn7mjEGS",
        "chapter": 5,
        "english": "come (location)",
        "kanji": "来ます",
        "verb_group": 1,
        "polite_form": {
            "present": "きます",
            "present_negative": "きません",
            "past": "きました",
            "past_negative": "きませんでした"
        },
        "plain_form": {
            "present": "くる",
            "present_negative": "かない",
            "past": "いた",
            "past_negative": "かなかった"
        },
        "te_form": {
            "present": "きて"
        }
    },
    {
        "id": "_z9RXC7LgP",
        "chapter": 5,
        "english": "return; go home",
        "kanji": "帰ります",
        "verb_group": 1,
        "polite_form": {
            "present": "かえります",
            "present_negative": "かえりません",
            "past": "かえりました",
            "past_negative": "かえりませんでした"
        },
        "plain_form": {
            "present": "かえる",
            "present_negative": "かえらない",
            "past": "かえった",
            "past_negative": "かえらなかった"
        },
        "te_form": {
            "present": "かえって"
        }
    },
    {
        "id": "itaJkn2L_",
        "chapter": 6,
        "english": "eat",
        "kanji": "食べます",
        "verb_group": 2,
        "polite_form": {
            "present": "たべます",
            "present_negative": "たべません",
            "past": "たべました",
            "past_negative": "たべませんでした"
        },
        "plain_form": {
            "present": "たべる",
            "present_negative": "たべない",
            "past": "たべた",
            "past_negative": "たべなかった"
        },
        "te_form": {
            "present": "たべて"
        }
    },
    {
        "id": "Xaj6nka73c",
        "chapter": 6,
        "english": "drink",
        "kanji": "飲みます",
        "verb_group": 1,
        "polite_form": {
            "present": "のみます",
            "present_negative": "のみません",
            "past": "のみました",
            "past_negative": "のみませんでした"
        },
        "plain_form": {
            "present": "のむ",
            "present_negative": "のまない",
            "past": "のんだ",
            "past_negative": "のまなかった"
        },
        "te_form": {
            "present": "のんで"
        }
    },
    {
        "id": "rlE_1a8HG3",
        "chapter": 6,
        "english": "smoke",
        "kanji": "吸います",
        "verb_group": 1,
        "polite_form": {
            "present": "すいます",
            "present_negative": "すいません",
            "past": "すいました",
            "past_negative": "すいませんでした"
        },
        "plain_form": {
            "present": "すう",
            "present_negative": "すわない",
            "past": "すった",
            "past_negative": "すわなかった"
        },
        "te_form": {
            "present": "すって"
        }
    },
    {
        "id": "DjIkR-EErJ",
        "chapter": 6,
        "english": "watch; see",
        "kanji": "見ます",
        "verb_group": 2,
        "polite_form": {
            "present": "みます",
            "present_negative": "みません",
            "past": "みました",
            "past_negative": "みませんでした"
        },
        "plain_form": {
            "present": "みる",
            "present_negative": "みない",
            "past": "みた",
            "past_negative": "みなかった"
        },
        "te_form": {
            "present": "みて"
        }
    },
    {
        "id": "NQ9nySgNnf",
        "chapter": 6,
        "english": "hear; listen",
        "kanji": "聞きます",
        "verb_group": 2,
        "polite_form": {
            "present": "ききます",
            "present_negative": "ききません",
            "past": "ききました",
            "past_negative": "ききませんでした"
        },
        "plain_form": {
            "present": "きく",
            "present_negative": "きかない",
            "past": "きいた",
            "past_negative": "きかなかった"
        },
        "te_form": {
            "present": "きいて"
        }
    },
    {
        "id": "TdjWnGsF2G",
        "chapter": 6,
        "english": "read",
        "kanji": "読みます",
        "verb_group": 1,
        "polite_form": {
            "present": "よみます",
            "present_negative": "よみません",
            "past": "よみました",
            "past_negative": "よみませんでした"
        },
        "plain_form": {
            "present": "よむ",
            "present_negative": "よまない",
            "past": "よんだ",
            "past_negative": "よまなかった"
        },
        "te_form": {
            "present": "よんで"
        }
    },
    {
        "id": "dJI2u02lIg",
        "chapter": 6,
        "english": "write; paint",
        "kanji": "描きます",
        "verb_group": 1,
        "polite_form": {
            "present": "かきます",
            "present_negative": "かきません",
            "past": "かきました",
            "past_negative": "かきませんでした"
        },
        "plain_form": {
            "present": "かく",
            "present_negative": "かかない",
            "past": "かいた",
            "past_negative": "かかなかった"
        },
        "te_form": {
            "present": "かいて"
        }
    },
    {
        "id": "ZRowY5FGGyu",
        "chapter": 6,
        "english": "buy",
        "kanji": "買います",
        "verb_group": 1,
        "polite_form": {
            "present": "かいます",
            "present_negative": "かいません",
            "past": "かいました",
            "past_negative": "かいませんでした"
        },
        "plain_form": {
            "present": "かう",
            "present_negative": "かわない",
            "past": "かった",
            "past_negative": "かわなかった"
        },
        "te_form": {
            "present": "かって"
        }
    },
    {
        "id": "2gtqxhT4fHb",
        "chapter": 6,
        "english": "photograph",
        "kanji": "撮ります",
        "verb_group": 1,
        "polite_form": {
            "present": "とります",
            "present_negative": "とりません",
            "past": "とりました",
            "past_negative": "とりませんでした"
        },
        "plain_form": {
            "present": "とる",
            "present_negative": "とらない",
            "past": "とった",
            "past_negative": "とらなかった"
        },
        "te_form": {
            "present": "とって"
        }
    },
    {
        "id": "6vT8LbmGjYQ",
        "chapter": 6,
        "english": "do",
        "kanji": "します",
        "verb_group": 3,
        "polite_form": {
            "present": "します",
            "present_negative": "しません",
            "past": "しました",
            "past_negative": "しませんでした"
        },
        "plain_form": {
            "present": "する",
            "present_negative": "しない",
            "past": "した",
            "past_negative": "しなかった"
        },
        "te_form": {
            "present": "して"
        }
    },
    {
        "id": "BGmxB6QNhzG-",
        "chapter": 6,
        "english": "meet (a friend)",
        "kanji": "会います",
        "verb_group": 1,
        "polite_form": {
            "present": "あいます",
            "present_negative": "あいません",
            "past": "あいました",
            "past_negative": "あいませんでした"
        },
        "plain_form": {
            "present": "あう",
            "present_negative": "あわない",
            "past": "あった",
            "past_negative": "あわなかった"
        },
        "te_form": {
            "present": "あって"
        }
    },
    {
        "id": "W4khWfj_K6E",
        "chapter": 7,
        "english": "cut; slice",
        "kanji": "切ります",
        "verb_group": 2,
        "polite_form": {
            "present": "きります",
            "present_negative": "きりません",
            "past": "きりました",
            "past_negative": "きりませんでした"
        },
        "plain_form": {
            "present": "きりる",
            "present_negative": "きりない",
            "past": "きりた",
            "past_negative": "きりなかった"
        },
        "te_form": {
            "present": "きって"
        }
    },
    {
        "id": "IcZAUwBywzo",
        "chapter": 7,
        "english": "send",
        "kanji": "送ります",
        "verb_group": 1,
        "polite_form": {
            "present": "おくります",
            "present_negative": "おくりません",
            "past": "おくりました",
            "past_negative": "おくりませんでした"
        },
        "plain_form": {
            "present": "おくる",
            "present_negative": "おくらない",
            "past": "おくった",
            "past_negative": "おくらなかった"
        },
        "te_form": {
            "present": "おくって"
        }
    },
    {
        "id": "zGhqorQmnje",
        "chapter": 7,
        "english": "give",
        "kanji": "上げます",
        "verb_group": 2,
        "polite_form": {
            "present": "あげます",
            "present_negative": "あげません",
            "past": "あげました",
            "past_negative": "あげませんでした"
        },
        "plain_form": {
            "present": "あげる",
            "present_negative": "あげない",
            "past": "あげた",
            "past_negative": "あげなかった"
        },
        "te_form": {
            "present": "あげて"
        }
    },
    {
        "id": "cAMiGwR2wuG",
        "chapter": 7,
        "english": "receive",
        "kanji": "貰います",
        "verb_group": 1,
        "polite_form": {
            "present": "もらいます",
            "present_negative": "もらいません",
            "past": "もらいました",
            "past_negative": "もらいませんでした"
        },
        "plain_form": {
            "present": "もらう",
            "present_negative": "もらわない",
            "past": "もらった",
            "past_negative": "もらわなかった"
        },
        "te_form": {
            "present": "もらって"
        }
    },
    {
        "id": "BVVpkGuMMJQ",
        "chapter": 7,
        "english": "lend",
        "kanji": "貸します",
        "verb_group": 1,
        "polite_form": {
            "present": "かします",
            "present_negative": "かしません",
            "past": "かしました",
            "past_negative": "かしませんでした"
        },
        "plain_form": {
            "present": "かす",
            "present_negative": "かちない",
            "past": "かした",
            "past_negative": "かちなかった"
        },
        "te_form": {
            "present": "かして"
        }
    },
    {
        "id": "T9LbT-APJEF",
        "chapter": 7,
        "english": "borrow",
        "kanji": "借ります",
        "verb_group": 2,
        "polite_form": {
            "present": "かります",
            "present_negative": "かりません",
            "past": "かりました",
            "past_negative": "かりませんでした"
        },
        "plain_form": {
            "present": "かりる",
            "present_negative": "かりない",
            "past": "かりた",
            "past_negative": "かりなかった"
        },
        "te_form": {
            "present": "かりて"
        }
    },
    {
        "id": "JSvf9i0d7pt-J",
        "chapter": 7,
        "english": "teach",
        "kanji": "教えます",
        "verb_group": 2,
        "polite_form": {
            "present": "おしえます",
            "present_negative": "おしえません",
            "past": "おしえました",
            "past_negative": "おしえませんでした"
        },
        "plain_form": {
            "present": "おしえる",
            "present_negative": "おしえない",
            "past": "おしえた",
            "past_negative": "おしえなかった"
        },
        "te_form": {
            "present": "おしえて"
        }
    },
    {
        "id": "zBikfFq4bSM",
        "chapter": 7,
        "english": "learn",
        "kanji": "習います",
        "verb_group": 1,
        "polite_form": {
            "present": "ならいます",
            "present_negative": "ならいません",
            "past": "ならいました",
            "past_negative": "ならいませんでした"
        },
        "plain_form": {
            "present": "ならう",
            "present_negative": "ならわない",
            "past": "ならった",
            "past_negative": "ならわなかった"
        },
        "te_form": {
            "present": "ならって"
        }
    },
    {
        "id": "EI5WMQDmB3b",
        "chapter": 7,
        "english": "make a call",
        "kanji": "掛けます",
        "verb_group": 2,
        "polite_form": {
            "present": "かけます",
            "present_negative": "かけません",
            "past": "かけました",
            "past_negative": "かけませんでした"
        },
        "plain_form": {
            "present": "かける",
            "present_negative": "かかない",
            "past": "かけた",
            "past_negative": "かかなかった"
        },
        "te_form": {
            "present": "かけて"
        }
    },
    {
        "id": "spO-pIL3UXs",
        "chapter": 8,
        "english": "understand",
        "kanji": "分かります",
        "verb_group": 1,
        "polite_form": {
            "present": "わかります",
            "present_negative": "わかりません",
            "past": "わかりました",
            "past_negative": "わかりませんでした"
        },
        "plain_form": {
            "present": "わかる",
            "present_negative": "わからない",
            "past": "わかった",
            "past_negative": "わからなかった"
        },
        "te_form": {
            "present": "わかりまして"
        }
    },
    {
        "id": "BeZE20T0ARv",
        "chapter": 8,
        "english": "have",
        "kanji": "ある",
        "verb_group": 1,
        "polite_form": {
            "present": "あります",
            "present_negative": "ありません",
            "past": "ありました",
            "past_negative": "ありませんでした"
        },
        "plain_form": {
            "present": "ある",
            "present_negative": "あらない",
            "past": "あった",
            "past_negative": "あらなかった"
        },
        "te_form": {
            "present": "あって"
        }
    },
    {
        "id": "8ksRaniFi7V-W",
        "chapter": 11,
        "english": "cost (time / money)",
        "kanji": "掛かります",
        "verb_group": 1,
        "polite_form": {
            "present": "かかります",
            "present_negative": "かかりません",
            "past": "かかりました",
            "past_negative": "かかりませんでした"
        },
        "plain_form": {
            "present": "かかる",
            "present_negative": "かからない",
            "past": "かかった",
            "past_negative": "かからなかった"
        },
        "te_form": {
            "present": "かかって"
        }
    },
    {
        "id": "PEe1QJ2cL0v",
        "chapter": 13,
        "english": "play",
        "kanji": "遊ぶ",
        "verb_group": 1,
        "polite_form": {
            "present": "あそびます",
            "present_negative": "あそびません",
            "past": "あそびました",
            "past_negative": "あそびませんでした"
        },
        "plain_form": {
            "present": "あそぶ",
            "present_negative": "あそばない",
            "past": "あそんだ",
            "past_negative": "あそばなかった"
        },
        "te_form": {
            "present": "あそんで"
        }
    },
    {
        "id": "B6hHSJiWAaX",
        "chapter": 13,
        "english": "swim",
        "kanji": "泳ぎます",
        "verb_group": 1,
        "polite_form": {
            "present": "およぎます",
            "present_negative": "およぎません",
            "past": "およぎました",
            "past_negative": "およぎませんでした"
        },
        "plain_form": {
            "present": "およぐ",
            "present_negative": "およがない",
            "past": "およいだ",
            "past_negative": "およがなかった"
        },
        "te_form": {
            "present": "およいで"
        }
    },
    {
        "id": "4OceJhrwxP2-",
        "chapter": 13,
        "english": "meet",
        "kanji": "迎えます",
        "verb_group": 2,
        "polite_form": {
            "present": "むかえます",
            "present_negative": "むかえません",
            "past": "むかえました",
            "past_negative": "むかえませんでした"
        },
        "plain_form": {
            "present": "むかえる",
            "present_negative": "むかえない",
            "past": "むかえた",
            "past_negative": "むかえなかった"
        },
        "te_form": {
            "present": "むかえて"
        }
    },
    {
        "id": "XA2G6az_BGp-",
        "chapter": 13,
        "english": "tire",
        "kanji": "疲れます",
        "verb_group": 2,
        "polite_form": {
            "present": "つかれます",
            "present_negative": "つかれません",
            "past": "つかれました",
            "past_negative": "つかれませんでした"
        },
        "plain_form": {
            "present": "つかれる",
            "present_negative": "つかれない",
            "past": "つかれた",
            "past_negative": "つかれなかった"
        },
        "te_form": {
            "present": "つかれて"
        }
    },
    {
        "id": "QUIZChpQLXc",
        "chapter": 13,
        "english": "walk",
        "kanji": "遊びます",
        "verb_group": 1,
        "polite_form": {
            "present": "あるびます",
            "present_negative": "あるびません",
            "past": "あるびました",
            "past_negative": "あるびませんでした"
        },
        "plain_form": {
            "present": "あるぶ",
            "present_negative": "あるばない",
            "past": "あるんだ",
            "past_negative": "あるばなかった"
        },
        "te_form": {
            "present": "あるんで"
        }
    },
    {
        "id": "jx2BZI4kW_R",
        "chapter": 13,
        "english": "shop; buy",
        "kanji": "買う",
        "verb_group": 1,
        "polite_form": {
            "present": "かいます",
            "present_negative": "かいません",
            "past": "かいました",
            "past_negative": "かいませんでした"
        },
        "plain_form": {
            "present": "かう",
            "present_negative": "かわない",
            "past": "かった",
            "past_negative": "かわなかった"
        },
        "te_form": {
            "present": "かって"
        }
    },
    {
        "id": "FbuFOUm6-DF",
        "chapter": 14,
        "english": "turn on",
        "kanji": "点けます",
        "verb_group": 2,
        "polite_form": {
            "present": "つけます",
            "present_negative": "つけません",
            "past": "つけました",
            "past_negative": "つけませんでした"
        },
        "plain_form": {
            "present": "つける",
            "present_negative": "つけない",
            "past": "つけた",
            "past_negative": "つけなかった"
        },
        "te_form": {
            "present": "つけて"
        }
    },
    {
        "id": "UX4JDSDOzlg",
        "chapter": 14,
        "english": "turn off",
        "kanji": "消します",
        "verb_group": 1,
        "polite_form": {
            "present": "けします",
            "present_negative": "けしません",
            "past": "けしました",
            "past_negative": "けしませんでした"
        },
        "plain_form": {
            "present": "けす",
            "present_negative": "けちない",
            "past": "けした",
            "past_negative": "けちなかった"
        },
        "te_form": {
            "present": "けして"
        }
    },
    {
        "id": "CgxmF5EzsrF",
        "chapter": 14,
        "english": "open",
        "kanji": "開けます",
        "verb_group": 2,
        "polite_form": {
            "present": "あけます",
            "present_negative": "あけません",
            "past": "あけました",
            "past_negative": "あけませんでした"
        },
        "plain_form": {
            "present": "あける",
            "present_negative": "あけない",
            "past": "あけた",
            "past_negative": "あけなかった"
        },
        "te_form": {
            "present": "あけて"
        }
    },
    {
        "id": "pPNOflRBfhL",
        "chapter": 14,
        "english": "close",
        "kanji": "閉めます",
        "verb_group": 2,
        "polite_form": {
            "present": "しめます",
            "present_negative": "しめません",
            "past": "しめました",
            "past_negative": "しめませんでした"
        },
        "plain_form": {
            "present": "しめる",
            "present_negative": "しめない",
            "past": "しめた",
            "past_negative": "しめなかった"
        },
        "te_form": {
            "present": "しめて"
        }
    },
    {
        "id": "08rbKZXmRc8",
        "chapter": 14,
        "english": "hurry",
        "kanji": "急ぎます",
        "verb_group": 1,
        "polite_form": {
            "present": "いそぎます",
            "present_negative": "いそぎません",
            "past": "いそぎました",
            "past_negative": "いそぎませんでした"
        },
        "plain_form": {
            "present": "いそぐ",
            "present_negative": "いそがない",
            "past": "いそいだ",
            "past_negative": "いそがなかった"
        },
        "te_form": {
            "present": "いそいで"
        }
    },
    {
        "id": "C0PcKhTYdYp",
        "chapter": 14,
        "english": "wait",
        "kanji": "待ちます",
        "verb_group": 1,
        "polite_form": {
            "present": "まちます",
            "present_negative": "まちません",
            "past": "まちました",
            "past_negative": "まちませんでした"
        },
        "plain_form": {
            "present": "まつ",
            "present_negative": "またない",
            "past": "まった",
            "past_negative": "またなかった"
        },
        "te_form": {
            "present": "まって"
        }
    },
    {
        "id": "vjdYNM8KQ42",
        "chapter": 14,
        "english": "hold",
        "kanji": "持ちます",
        "verb_group": 1,
        "polite_form": {
            "present": "もちます",
            "present_negative": "もちません",
            "past": "もちました",
            "past_negative": "もちませんでした"
        },
        "plain_form": {
            "present": "もつ",
            "present_negative": "もたない",
            "past": "もった",
            "past_negative": "もたなかった"
        },
        "te_form": {
            "present": "もって"
        }
    },
    {
        "id": "7BoSXVvD1_D",
        "chapter": 14,
        "english": "take; pass",
        "kanji": "取ります",
        "verb_group": 1,
        "polite_form": {
            "present": "とります",
            "present_negative": "とりません",
            "past": "とりました",
            "past_negative": "とりませんでした"
        },
        "plain_form": {
            "present": "とる",
            "present_negative": "とらない",
            "past": "とった",
            "past_negative": "とらなかった"
        },
        "te_form": {
            "present": "とって"
        }
    },
    {
        "id": "7PQ4eczc4SX",
        "chapter": 14,
        "english": "help",
        "kanji": "手伝います",
        "verb_group": 1,
        "polite_form": {
            "present": "てつだいます",
            "present_negative": "てつだいません",
            "past": "てつだいました",
            "past_negative": "てつだいませんでした"
        },
        "plain_form": {
            "present": "てつだう",
            "present_negative": "てつだわない",
            "past": "てつだった",
            "past_negative": "てつだわなかった"
        },
        "te_form": {
            "present": "てつだって"
        }
    },
    {
        "id": "Q-EoEQ765sx",
        "chapter": 14,
        "english": "call",
        "kanji": "呼びます",
        "verb_group": 1,
        "polite_form": {
            "present": "よびます",
            "present_negative": "よびません",
            "past": "よびました",
            "past_negative": "よびませんでした"
        },
        "plain_form": {
            "present": "よぶ",
            "present_negative": "よばない",
            "past": "よんだ",
            "past_negative": "よばなかった"
        },
        "te_form": {
            "present": "よんで"
        }
    },
    {
        "id": "n7Haw4wf_YS",
        "chapter": 14,
        "english": "speak",
        "kanji": "話します",
        "verb_group": 1,
        "polite_form": {
            "present": "はなします",
            "present_negative": "はなしません",
            "past": "はなしました",
            "past_negative": "はなしませんでした"
        },
        "plain_form": {
            "present": "はなす",
            "present_negative": "はなちない",
            "past": "はなした",
            "past_negative": "はなちなかった"
        },
        "te_form": {
            "present": "はなして"
        }
    },
    {
        "id": "RvTc7ItkuV_",
        "chapter": 14,
        "english": "use",
        "kanji": "使います",
        "verb_group": 1,
        "polite_form": {
            "present": "つかいます",
            "present_negative": "つかいません",
            "past": "つかいました",
            "past_negative": "つかいませんでした"
        },
        "plain_form": {
            "present": "つかう",
            "present_negative": "つかわない",
            "past": "つかった",
            "past_negative": "つかわなかった"
        },
        "te_form": {
            "present": "つかって"
        }
    },
    {
        "id": "qSMjvDurQUx",
        "chapter": 14,
        "english": "stop; park",
        "kanji": "止めます",
        "verb_group": 2,
        "polite_form": {
            "present": "とめます",
            "present_negative": "とめません",
            "past": "とめました",
            "past_negative": "とめませんでした"
        },
        "plain_form": {
            "present": "とめる",
            "present_negative": "とめない",
            "past": "とめた",
            "past_negative": "とめなかった"
        },
        "te_form": {
            "present": "とめて"
        }
    },
    {
        "id": "13DeOwH6ZkV",
        "chapter": 14,
        "english": "show",
        "kanji": "見せます",
        "verb_group": 2,
        "polite_form": {
            "present": "みせます",
            "present_negative": "みせません",
            "past": "みせました",
            "past_negative": "みせませんでした"
        },
        "plain_form": {
            "present": "みせる",
            "present_negative": "みせない",
            "past": "みせた",
            "past_negative": "みせなかった"
        },
        "te_form": {
            "present": "みせて"
        }
    },
    {
        "id": "giK8s0vmSFh-",
        "chapter": 14,
        "english": "tell",
        "kanji": "教えます",
        "verb_group": 2,
        "polite_form": {
            "present": "おしえます",
            "present_negative": "おしえません",
            "past": "おしえました",
            "past_negative": "おしえませんでした"
        },
        "plain_form": {
            "present": "おしえる",
            "present_negative": "おしえない",
            "past": "おしえた",
            "past_negative": "おしえなかった"
        },
        "te_form": {
            "present": "おしえて"
        }
    },
    {
        "id": "oT9HKYi2W",
        "chapter": 14,
        "english": "sit",
        "kanji": "座ります",
        "verb_group": 1,
        "polite_form": {
            "present": "すわります",
            "present_negative": "すわりません",
            "past": "すわりました",
            "past_negative": "すわりませんでした"
        },
        "plain_form": {
            "present": "すわる",
            "present_negative": "すわるない",
            "past": "すわった",
            "past_negative": "すわるなかった"
        },
        "te_form": {
            "present": "すわって"
        }
    },
    {
        "id": "CP82udfL7",
        "chapter": 14,
        "english": "stand",
        "kanji": "立ちます",
        "verb_group": 1,
        "polite_form": {
            "present": "たちます",
            "present_negative": "たちません",
            "past": "たちました",
            "past_negative": "たちませんでした"
        },
        "plain_form": {
            "present": "たつ",
            "present_negative": "たたない",
            "past": "たった",
            "past_negative": "たたなかった"
        },
        "te_form": {
            "present": "たって"
        }
    },
    {
        "id": "SZKcO7dCMQh-lr0MvUy9",
        "chapter": 15,
        "english": "put, place",
        "kanji": "置きます",
        "verb_group": 1,
        "polite_form": {
            "present": "おきます",
            "present_negative": "おきません",
            "past": "おきました",
            "past_negative": "おきませんでした"
        },
        "plain_form": {
            "present": "おく",
            "present_negative": "おかない",
            "past": "おいた",
            "past_negative": "おかなかった"
        },
        "te_form": {
            "present": "おきて"
        }
    },
    {
        "id": "WjX8CYW7Xc_",
        "chapter": 15,
        "english": "make; produce",
        "kanji": "作ります",
        "verb_group": 1,
        "polite_form": {
            "present": "つくります",
            "present_negative": "つくりません",
            "past": "つくりました",
            "past_negative": "つくりませんでした"
        },
        "plain_form": {
            "present": "つくる",
            "present_negative": "つくらない",
            "past": "つくった",
            "past_negative": "つくらなかった"
        },
        "te_form": {
            "present": "つくって"
        }
    },
    {
        "id": "JsBhfH3BOoA",
        "chapter": 15,
        "english": "sell",
        "kanji": "売ります",
        "verb_group": 1,
        "polite_form": {
            "present": "うります",
            "present_negative": "うりません",
            "past": "うりました",
            "past_negative": "うりませんでした"
        },
        "plain_form": {
            "present": "うる",
            "present_negative": "うらない",
            "past": "うった",
            "past_negative": "うらなかった"
        },
        "te_form": {
            "present": "うって"
        }
    },
    {
        "id": "Fs8UEjm86R4",
        "chapter": 15,
        "english": "know",
        "kanji": "知ります",
        "verb_group": 1,
        "polite_form": {
            "present": "しります",
            "present_negative": "しりません",
            "past": "しりました",
            "past_negative": "しりませんでした"
        },
        "plain_form": {
            "present": "しる",
            "present_negative": "しらない",
            "past": "しった",
            "past_negative": "しらなかった"
        },
        "te_form": {
            "present": "しって"
        }
    },
    {
        "id": "eWnuV_g7D7h",
        "chapter": 15,
        "english": "be going to live",
        "kanji": "住みます",
        "verb_group": 1,
        "polite_form": {
            "present": "すみます",
            "present_negative": "すみません",
            "past": "すみました",
            "past_negative": "すみませんでした"
        },
        "plain_form": {
            "present": "すむ",
            "present_negative": "すまない",
            "past": "すんだ",
            "past_negative": "すまなかった"
        },
        "te_form": {
            "present": "すんで"
        }
    },
    {
        "id": "Ro7hlaviz17-fSC2T-",
        "chapter": 15,
        "english": "research",
        "kanji": "研究します",
        "verb_group": 3,
        "polite_form": {
            "present": "けんきょう します",
            "present_negative": "けんきょう しません",
            "past": "けんきょう しました",
            "past_negative": "けんきょう しませんでした"
        },
        "plain_form": {
            "present": "けんきょう する",
            "present_negative": "けんきょう しない",
            "past": "けんきょう した",
            "past_negative": "けんきょう しなかった"
        },
        "te_form": {
            "present": "けんきょう して"
        }
    },
    {
        "id": "NASKyif330d-NXZ",
        "chapter": 16,
        "english": "get on (vehicle)",
        "kanji": "おります",
        "verb_group": 1,
        "polite_form": {
            "present": "のります",
            "present_negative": "のりません",
            "past": "のりました",
            "past_negative": "のりませんでした"
        },
        "plain_form": {
            "present": "のる",
            "present_negative": "のらない",
            "past": "のった",
            "past_negative": "のらなかった"
        },
        "te_form": {
            "present": "のって"
        }
    },
    {
        "id": "dQvy5j1PfPQ",
        "chapter": 16,
        "english": "get off (vehicle)",
        "kanji": "降ります",
        "verb_group": 2,
        "polite_form": {
            "present": "おります",
            "present_negative": "おりません",
            "past": "おりました",
            "past_negative": "おりませんでした"
        },
        "plain_form": {
            "present": "おりる",
            "present_negative": "おりない",
            "past": "おりた",
            "past_negative": "おりなかった"
        },
        "te_form": {
            "present": "おりて"
        }
    },
    {
        "id": "t5-dXlhkZs9",
        "chapter": 16,
        "english": "change (vehicles)",
        "kanji": "乗り換えます",
        "verb_group": 2,
        "polite_form": {
            "present": "のりかえます",
            "present_negative": "のりかえません",
            "past": "のりかえました",
            "past_negative": "のりかえませんでした"
        },
        "plain_form": {
            "present": "のりかえる",
            "present_negative": "のりかえない",
            "past": "のりかえた",
            "past_negative": "のりかえなかった"
        },
        "te_form": {
            "present": "のりかえて"
        }
    },
    {
        "id": "bACjrhPn-CT",
        "chapter": 16,
        "english": "take (a shower)",
        "kanji": "浴びます",
        "verb_group": 2,
        "polite_form": {
            "present": "あびます",
            "present_negative": "あびません",
            "past": "あびました",
            "past_negative": "あびませんでした"
        },
        "plain_form": {
            "present": "あびる",
            "present_negative": "あびない",
            "past": "あびた",
            "past_negative": "あびなかった"
        },
        "te_form": {
            "present": "あびて"
        }
    },
    {
        "id": "niHuQz9REVQ",
        "chapter": 16,
        "english": "put in; insert",
        "kanji": "入れます",
        "verb_group": 2,
        "polite_form": {
            "present": "いれます",
            "present_negative": "いれません",
            "past": "いれました",
            "past_negative": "いれませんでした"
        },
        "plain_form": {
            "present": "いれる",
            "present_negative": "いれない",
            "past": "いれた",
            "past_negative": "いれなかった"
        },
        "te_form": {
            "present": "いれて"
        }
    },
    {
        "id": "PdRUvNx4ejT",
        "chapter": 16,
        "english": "take out; get out",
        "kanji": "出す",
        "verb_group": 1,
        "polite_form": {
            "present": "だします",
            "present_negative": "だしません",
            "past": "だしました",
            "past_negative": "だしませんでした"
        },
        "plain_form": {
            "present": "だす",
            "present_negative": "だちない",
            "past": "だした",
            "past_negative": "だちなかった"
        },
        "te_form": {
            "present": "だして"
        }
    },
    {
        "id": "6y4E4cxlz3X",
        "chapter": 16,
        "english": "withdraw",
        "kanji": "下ろします",
        "verb_group": 1,
        "polite_form": {
            "present": "おろします",
            "present_negative": "おろしません",
            "past": "おろしました",
            "past_negative": "おろしませんでした"
        },
        "plain_form": {
            "present": "おろす",
            "present_negative": "おろちない",
            "past": "おろした",
            "past_negative": "おろちなかった"
        },
        "te_form": {
            "present": "おろして"
        }
    },
    {
        "id": "Foe0FQ4MChh-bca",
        "chapter": 16,
        "english": "enter (location)",
        "kanji": "入ります",
        "verb_group": 1,
        "polite_form": {
            "present": "はいります",
            "present_negative": "はいりません",
            "past": "はいりました",
            "past_negative": "はいりませんでした"
        },
        "plain_form": {
            "present": "はいる",
            "present_negative": "はいらない",
            "past": "はいった",
            "past_negative": "はいらなかった"
        },
        "te_form": {
            "present": "はいって"
        }
    },
    {
        "id": "atr9P2aXukr",
        "chapter": 16,
        "english": "leave (location)",
        "kanji": "出ます",
        "verb_group": 2,
        "polite_form": {
            "present": "でます",
            "present_negative": "でません",
            "past": "でました",
            "past_negative": "でませんでした"
        },
        "plain_form": {
            "present": "でる",
            "present_negative": "でない",
            "past": "でた",
            "past_negative": "でなかった"
        },
        "te_form": {
            "present": "でて"
        }
    },
    {
        "id": "Q8Ay9uv64Q2-b",
        "chapter": 16,
        "english": "push; press",
        "kanji": "押します",
        "verb_group": 1,
        "polite_form": {
            "present": "おします",
            "present_negative": "おしません",
            "past": "おしました",
            "past_negative": "おしませんでした"
        },
        "plain_form": {
            "present": "おす",
            "present_negative": "おちない",
            "past": "おした",
            "past_negative": "おちなかった"
        },
        "te_form": {
            "present": "おして"
        }
    },
    {
        "id": "Fl5gi-WHZwO",
        "chapter": 16,
        "english": "start; begin",
        "kanji": "始めます",
        "verb_group": 2,
        "polite_form": {
            "present": "はじめます",
            "present_negative": "はじめません",
            "past": "はじめました",
            "past_negative": "はじめませんでした"
        },
        "plain_form": {
            "present": "はじめる",
            "present_negative": "はじめない",
            "past": "はじめた",
            "past_negative": "はじめなかった"
        },
        "te_form": {
            "present": "はじめて"
        }
    },
    {
        "id": "48u1TOJMxnA",
        "chapter": 17,
        "english": "memorise",
        "kanji": "覚えます",
        "verb_group": 2,
        "polite_form": {
            "present": "おぽえます",
            "present_negative": "おぽえません",
            "past": "おぽえました",
            "past_negative": "おぽえませんでした"
        },
        "plain_form": {
            "present": "おぽえる",
            "present_negative": "おぽえない",
            "past": "おぽえた",
            "past_negative": "おぽえなかった"
        },
        "te_form": {
            "present": "おぽえて"
        }
    },
    {
        "id": "fw3Yer6QltH",
        "chapter": 17,
        "english": "forget",
        "kanji": "忘れます",
        "verb_group": 2,
        "polite_form": {
            "present": "わすれます",
            "present_negative": "わすれません",
            "past": "わすれました",
            "past_negative": "わすれませんでした"
        },
        "plain_form": {
            "present": "わすれる",
            "present_negative": "わすれない",
            "past": "わすれた",
            "past_negative": "わすれなかった"
        },
        "te_form": {
            "present": "わすれて"
        }
    },
    {
        "id": "ovLfXoWLJ7i",
        "chapter": 17,
        "english": "lose (item)",
        "kanji": "無くす",
        "verb_group": 1,
        "polite_form": {
            "present": "なくします",
            "present_negative": "なくしません",
            "past": "なくしました",
            "past_negative": "なくしませんでした"
        },
        "plain_form": {
            "present": "なくす",
            "present_negative": "なくさない",
            "past": "なくした",
            "past_negative": "なくさなかった"
        },
        "te_form": {
            "present": "なくして"
        }
    },
    {
        "id": "HZVIS34btYp",
        "chapter": 17,
        "english": "pay",
        "kanji": "払います",
        "verb_group": 1,
        "polite_form": {
            "present": "はらいます",
            "present_negative": "はらいません",
            "past": "はらいました",
            "past_negative": "はらいませんでした"
        },
        "plain_form": {
            "present": "はらう",
            "present_negative": "はらわない",
            "past": "はらった",
            "past_negative": "はらわなかった"
        },
        "te_form": {
            "present": "はらって"
        }
    },
    {
        "id": "K347S5Q69Hc-90fv",
        "chapter": 17,
        "english": "give back; return",
        "kanji": "返します",
        "verb_group": 1,
        "polite_form": {
            "present": "かえします",
            "present_negative": "かえしません",
            "past": "かえしました",
            "past_negative": "かえしませんでした"
        },
        "plain_form": {
            "present": "かえす",
            "present_negative": "かえちない",
            "past": "かえした",
            "past_negative": "かえちなかった"
        },
        "te_form": {
            "present": "かえして"
        }
    },
    {
        "id": "WoZmHbct7eb-bvuTAw",
        "chapter": 17,
        "english": "go out",
        "kanji": "出かけます",
        "verb_group": 2,
        "polite_form": {
            "present": "でかけます",
            "present_negative": "でかけません",
            "past": "でかけました",
            "past_negative": "でかけませんでした"
        },
        "plain_form": {
            "present": "でかける",
            "present_negative": "でかけない",
            "past": "でかけた",
            "past_negative": "でかけなかった"
        },
        "te_form": {
            "present": "でかけて"
        }
    },
    {
        "id": "p0Al7n_Ktau",
        "chapter": 17,
        "english": "undress",
        "kanji": "脱ぎます",
        "verb_group": 1,
        "polite_form": {
            "present": "ぬぎます",
            "present_negative": "ぬぎません",
            "past": "ぬぎました",
            "past_negative": "ぬぎませんでした"
        },
        "plain_form": {
            "present": "ぬぐ",
            "present_negative": "ぬぐない",
            "past": "ぬいだ",
            "past_negative": "ぬぐなかった"
        },
        "te_form": {
            "present": "ぬいで"
        }
    },
    {
        "id": "yQqWLqc2Jx-",
        "chapter": 17,
        "english": "take (thing)",
        "kanji": "持って 行きます",
        "verb_group": 1,
        "polite_form": {
            "present": "もって いきます",
            "present_negative": "もって いきません",
            "past": "もって いきました",
            "past_negative": "もって いきませんでした"
        },
        "plain_form": {
            "present": "もって いく",
            "present_negative": "もって いかない",
            "past": "もって いいた",
            "past_negative": "もって いかなかった"
        },
        "te_form": {
            "present": "もって いいて"
        }
    },
    {
        "id": "vItilBu6UwG",
        "chapter": 17,
        "english": "bring (thing)",
        "kanji": "持って 来ます",
        "verb_group": 3,
        "polite_form": {
            "present": "もって きます",
            "present_negative": "もって きません",
            "past": "もって きました",
            "past_negative": "もって きませんでした"
        },
        "plain_form": {
            "present": "もって くる",
            "present_negative": "もって こない",
            "past": "もって きた",
            "past_negative": "もって こなかった"
        },
        "te_form": {
            "present": "もって いて"
        }
    },
    {
        "id": "2ZaqlonLika",
        "chapter": 17,
        "english": "worry",
        "kanji": "心配 します",
        "verb_group": 3,
        "polite_form": {
            "present": "しんぱい します",
            "present_negative": "しんぱい しません",
            "past": "しんぱい しました",
            "past_negative": "しんぱい しませんでした"
        },
        "plain_form": {
            "present": "しんぱい する",
            "present_negative": "しんぱい しない",
            "past": "しんぱい した",
            "past_negative": "しんぱい しなかった"
        },
        "te_form": {
            "present": "しんぱい して"
        }
    },
    {
        "id": "HQF58PJ8xW6",
        "chapter": 17,
        "english": "work overtime",
        "kanji": "残業 します",
        "verb_group": 3,
        "polite_form": {
            "present": "ざんぎょう します",
            "present_negative": "ざんぎょう しません",
            "past": "ざんぎょう しました",
            "past_negative": "ざんぎょう しませんでした"
        },
        "plain_form": {
            "present": "ざんぎょう する",
            "present_negative": "ざんぎょう しない",
            "past": "ざんぎょう した",
            "past_negative": "ざんぎょう しなかった"
        },
        "te_form": {
            "present": "ざんぎょう して"
        }
    },
    {
        "id": "XbL2r07ebWQ-8ElxJeI",
        "chapter": 17,
        "english": "go on a business trip",
        "kanji": "出張 します",
        "verb_group": 3,
        "polite_form": {
            "present": "しゅっちょう します",
            "present_negative": "しゅっちょう しません",
            "past": "しゅっちょう しました",
            "past_negative": "しゅっちょう しませんでした"
        },
        "plain_form": {
            "present": "しゅっちょう する",
            "present_negative": "しゅっちょう しない",
            "past": "しゅっちょう した",
            "past_negative": "しゅっちょう しなかった"
        },
        "te_form": {
            "present": "しゅっちょう して"
        }
    },
    {
        "id": "fxMSAmeynLk",
        "chapter": 17,
        "english": "climb",
        "kanji": "上ります",
        "verb_group": 1,
        "polite_form": {
            "present": "のぼります",
            "present_negative": "のぼりません",
            "past": "のぼりました",
            "past_negative": "のぼりませんでした"
        },
        "plain_form": {
            "present": "のぼる",
            "present_negative": "のぼらない",
            "past": "のぼった",
            "past_negative": "のぼらなかった"
        },
        "te_form": {
            "present": "のぼって"
        }
    },
    {
        "id": "hCjRBOb4oGe",
        "chapter": 17,
        "english": "stop",
        "kanji": "止まります",
        "verb_group": 1,
        "polite_form": {
            "present": "とまります",
            "present_negative": "とまりません",
            "past": "とまりました",
            "past_negative": "とまりませんでした"
        },
        "plain_form": {
            "present": "とまる",
            "present_negative": "とまらない",
            "past": "とまった",
            "past_negative": "とまらなかった"
        },
        "te_form": {
            "present": "とまって"
        }
    },
    {
        "id": "Mkfh_sBtm_2-K",
        "chapter": 17,
        "english": "clean",
        "kanji": "掃除 します",
        "verb_group": 3,
        "polite_form": {
            "present": "そうじ します",
            "present_negative": "そうじ しません",
            "past": "そうじ しました",
            "past_negative": "そうじ しませんでした"
        },
        "plain_form": {
            "present": "そうじ する",
            "present_negative": "そうじ しない",
            "past": "そうじ した",
            "past_negative": "そうじ しなかった"
        },
        "te_form": {
            "present": "そうじ して"
        }
    },
    {
        "id": "VOREOBpOvxo",
        "chapter": 17,
        "english": "wash",
        "kanji": "洗濯 します",
        "verb_group": 3,
        "polite_form": {
            "present": "せんたく します",
            "present_negative": "せんたく しません",
            "past": "せんたく しました",
            "past_negative": "せんたく しませんでした"
        },
        "plain_form": {
            "present": "せんたく する",
            "present_negative": "せんたく しない",
            "past": "せんたく した",
            "past_negative": "せんたく しなかった"
        },
        "te_form": {
            "present": "せんたく して"
        }
    },
    {
        "id": "c4v3956a73z",
        "chapter": 17,
        "english": "become",
        "kanji": "成ります",
        "verb_group": 1,
        "polite_form": {
            "present": "なります",
            "present_negative": "なりません",
            "past": "なりました",
            "past_negative": "なりませんでした"
        },
        "plain_form": {
            "present": "なる",
            "present_negative": "ならない",
            "past": "なった",
            "past_negative": "ならなかった"
        },
        "te_form": {
            "present": "なって"
        }
    },
    {
        "id": "_KlP0iXDr",
        "chapter": 18,
        "english": "be able to",
        "kanji": "出来ます",
        "verb_group": 2,
        "polite_form": {
            "present": "できます",
            "present_negative": "できません",
            "past": "できました",
            "past_negative": "できませんでした"
        },
        "plain_form": {
            "present": "できる",
            "present_negative": "できない",
            "past": "できた",
            "past_negative": "できなかった"
        },
        "te_form": {
            "present": "できて"
        }
    },
    {
        "id": "JfilNaNf2",
        "chapter": 18,
        "english": "wash",
        "kanji": "洗います",
        "verb_group": 1,
        "polite_form": {
            "present": "あらいます",
            "present_negative": "あらいません",
            "past": "あらいました",
            "past_negative": "あらいませんでした"
        },
        "plain_form": {
            "present": "あらう",
            "present_negative": "あるわない",
            "past": "あらった",
            "past_negative": "あるわなかった"
        },
        "te_form": {
            "present": "あらって"
        }
    },
    {
        "id": "86NaMPvqY",
        "chapter": 18,
        "english": "play (instrument)",
        "kanji": "引きます",
        "verb_group": 1,
        "polite_form": {
            "present": "ひきます",
            "present_negative": "ひきません",
            "past": "ひきました",
            "past_negative": "ひきませんでした"
        },
        "plain_form": {
            "present": "ひく",
            "present_negative": "ひかない",
            "past": "ひいた",
            "past_negative": "ひかなかった"
        },
        "te_form": {
            "present": "ひいて"
        }
    },
    {
        "id": "q6aeyugnj",
        "chapter": 18,
        "english": "sing",
        "kanji": "歌います",
        "verb_group": 1,
        "polite_form": {
            "present": "うたいます",
            "present_negative": "うたいません",
            "past": "うたいました",
            "past_negative": "うたいませんでした"
        },
        "plain_form": {
            "present": "うたう",
            "present_negative": "うたわない",
            "past": "うたった",
            "past_negative": "うたわなかった"
        },
        "te_form": {
            "present": "うたって"
        }
    },
    {
        "id": "OnEjckiid",
        "chapter": 18,
        "english": "collect",
        "kanji": "集めます",
        "verb_group": 2,
        "polite_form": {
            "present": "あつめます",
            "present_negative": "あつめません",
            "past": "あつめました",
            "past_negative": "あつめませんでした"
        },
        "plain_form": {
            "present": "あつめる",
            "present_negative": "あつめない",
            "past": "あつめた",
            "past_negative": "あつめなかった"
        },
        "te_form": {
            "present": "あつめて"
        }
    },
    {
        "id": "x0Eb6mhSu",
        "chapter": 18,
        "english": "throw away",
        "kanji": "捨てます",
        "verb_group": 2,
        "polite_form": {
            "present": "すてます",
            "present_negative": "すてません",
            "past": "すてました",
            "past_negative": "すてませんでした"
        },
        "plain_form": {
            "present": "すてる",
            "present_negative": "すてない",
            "past": "すてた",
            "past_negative": "すてなかった"
        },
        "te_form": {
            "present": "すてて"
        }
    },
    {
        "id": "J4axf8yoV",
        "chapter": 18,
        "english": "drive (vehicle)",
        "kanji": "運転",
        "verb_group": 3,
        "polite_form": {
            "present": "うんてんします",
            "present_negative": "うんてんしません",
            "past": "うんてんしました",
            "past_negative": "うんてんしませんでした"
        },
        "plain_form": {
            "present": "うんてんする",
            "present_negative": "うんてんさない",
            "past": "うんてんした",
            "past_negative": "うんてんさなかった"
        },
        "te_form": {
            "present": "うんてんして"
        }
    },
    {
        "id": "yeVs4IxgX",
        "chapter": 18,
        "english": "reserve",
        "kanji": "予約します",
        "verb_group": 3,
        "polite_form": {
            "present": "よやくします",
            "present_negative": "よやくしません",
            "past": "よやくしました",
            "past_negative": "よやくしませんでした"
        },
        "plain_form": {
            "present": "よやくする",
            "present_negative": "よやくさない",
            "past": "よやくした",
            "past_negative": "よやくさなかった"
        },
        "te_form": {
            "present": "よやくして"
        }
    },
    {
        "id": "1632tDmvA",
        "chapter": 19,
        "english": "climb",
        "kanji": "上ります",
        "verb_group": 1,
        "polite_form": {
            "present": "のぼります",
            "present_negative": "のぼりません",
            "past": "のぼりました",
            "past_negative": "のぼりませんでした"
        },
        "plain_form": {
            "present": "のぼる",
            "present_negative": "のぼらない",
            "past": "のぼった",
            "past_negative": "のぼらなかった"
        },
        "te_form": {
            "present": "のぼって"
        }
    },
    {
        "id": "iVn72IJyV",
        "chapter": 19,
        "english": "stay (hotel)",
        "kanji": "泊まります",
        "verb_group": 1,
        "polite_form": {
            "present": "とまります",
            "present_negative": "とまりません",
            "past": "とまりました",
            "past_negative": "とまりませんでした"
        },
        "plain_form": {
            "present": "とまる",
            "present_negative": "とまらない",
            "past": "とまった",
            "past_negative": "とまらなかった"
        },
        "te_form": {
            "present": "とまって"
        }
    },
    {
        "id": "ztu4W-NpI",
        "chapter": 19,
        "english": "clean (home)",
        "kanji": "掃除します",
        "verb_group": 3,
        "polite_form": {
            "present": "そうじします",
            "present_negative": "そうじしません",
            "past": "そうじしました",
            "past_negative": "そうじしませんでした"
        },
        "plain_form": {
            "present": "そうじする",
            "present_negative": "そうじさない",
            "past": "そうじした",
            "past_negative": "そうじさなかった"
        },
        "te_form": {
            "present": "そうじして"
        }
    },
    {
        "id": "EjuMulI1R",
        "chapter": 19,
        "english": "wash (clothes)",
        "kanji": "洗濯します",
        "verb_group": 3,
        "polite_form": {
            "present": "せんたくします",
            "present_negative": "せんたくしません",
            "past": "せんたくしました",
            "past_negative": "せんたくしませんでした"
        },
        "plain_form": {
            "present": "せんたくする",
            "present_negative": "せんたくさない",
            "past": "せんたくした",
            "past_negative": "せんたくさなかった"
        },
        "te_form": {
            "present": "せんたくして"
        }
    },
    {
        "id": "otiCMQ893",
        "chapter": 19,
        "english": "become",
        "kanji": "なります",
        "verb_group": 1,
        "polite_form": {
            "present": "なります",
            "present_negative": "なりません",
            "past": "なりました",
            "past_negative": "なりませんでした"
        },
        "plain_form": {
            "present": "なる",
            "present_negative": "ならない",
            "past": "なった",
            "past_negative": "ならなかった"
        },
        "te_form": {
            "present": "なって"
        }
    },
    {
        "id": "lGaLp9PgFXI",
        "chapter": 20,
        "english": "need; require",
        "kanji": "要ります",
        "verb_group": 1,
        "polite_form": {
            "present": "いります",
            "present_negative": "いりません",
            "past": "いりました",
            "past_negative": "いりませんでした"
        },
        "plain_form": {
            "present": "いる",
            "present_negative": "いらない",
            "past": "いった",
            "past_negative": "いらなかった"
        },
        "te_form": {
            "present": "いって"
        }
    },
    {
        "id": "iga7VMgKqg3",
        "chapter": 20,
        "english": "check; investigate",
        "kanji": "調べます",
        "verb_group": 2,
        "polite_form": {
            "present": "しらべます",
            "present_negative": "しらべません",
            "past": "しらべました",
            "past_negative": "しらべませんでした"
        },
        "plain_form": {
            "present": "しらべる",
            "present_negative": "しらべない",
            "past": "しらべた",
            "past_negative": "しらべなかった"
        },
        "te_form": {
            "present": "しらべて"
        }
    },
    {
        "id": "6xDLixmK-KX",
        "chapter": 20,
        "english": "repair",
        "kanji": "修理します",
        "verb_group": 3,
        "polite_form": {
            "present": "しゅうりします",
            "present_negative": "しゅうりしません",
            "past": "しゅうりしました",
            "past_negative": "しゅうりしませんでした"
        },
        "plain_form": {
            "present": "しゅうりする",
            "present_negative": "しゅうりしない",
            "past": "しゅうりした",
            "past_negative": "しゅうりしなかった"
        },
        "te_form": {
            "present": "しゅうりして"
        }
    },
    {
        "id": "W-KEiY7vqOc",
        "chapter": 21,
        "english": "think",
        "kanji": "思います",
        "verb_group": 1,
        "polite_form": {
            "present": "おもいます",
            "present_negative": "おもいません",
            "past": "おもいました",
            "past_negative": "おもいませんでした"
        },
        "plain_form": {
            "present": "おもう",
            "present_negative": "おもわない",
            "past": "おもった",
            "past_negative": "おもわなかった"
        },
        "te_form": {
            "present": "おもって"
        }
    },
    {
        "id": "wd0RUcYGVvP",
        "chapter": 21,
        "english": "say",
        "kanji": "言います",
        "verb_group": 1,
        "polite_form": {
            "present": "いいます",
            "present_negative": "いいません",
            "past": "いいました",
            "past_negative": "いいませんでした"
        },
        "plain_form": {
            "present": "いう",
            "present_negative": "いわない",
            "past": "いった",
            "past_negative": "いわなかった"
        },
        "te_form": {
            "present": "いって"
        }
    },
    {
        "id": "Zxk9tjW76u5",
        "chapter": 21,
        "english": "win",
        "kanji": "勝ちます",
        "verb_group": 1,
        "polite_form": {
            "present": "かちます",
            "present_negative": "かちません",
            "past": "かちました",
            "past_negative": "かちませんでした"
        },
        "plain_form": {
            "present": "かつ",
            "present_negative": "かたない",
            "past": "かった",
            "past_negative": "かたなかった"
        },
        "te_form": {
            "present": "かって"
        }
    },
    {
        "id": "dPXqxa-qFWv",
        "chapter": 21,
        "english": "lose",
        "kanji": "負けます",
        "verb_group": 2,
        "polite_form": {
            "present": "まけます",
            "present_negative": "まけません",
            "past": "まけました",
            "past_negative": "まけませんでした"
        },
        "plain_form": {
            "present": "まける",
            "present_negative": "まけない",
            "past": "まけた",
            "past_negative": "まけなかった"
        },
        "te_form": {
            "present": "まけて"
        }
    },
    {
        "id": "o619QtZ0avO",
        "chapter": 21,
        "english": "be useful",
        "kanji": "役に 立ちます",
        "verb_group": 1,
        "polite_form": {
            "present": "やくに たちます",
            "present_negative": "やくに たちません",
            "past": "やくに たちました",
            "past_negative": "やくに たちませんでした"
        },
        "plain_form": {
            "present": "やくに たつ",
            "present_negative": "やくに たたない",
            "past": "やくに たった",
            "past_negative": "やくに たたなかった"
        },
        "te_form": {
            "present": "やくに たって"
        }
    },
    {
        "id": "auJPJ3DgL6O",
        "chapter": 21,
        "english": "move; work",
        "kanji": "動きます",
        "verb_group": 1,
        "polite_form": {
            "present": "うごきます",
            "present_negative": "うごきません",
            "past": "うごきました",
            "past_negative": "うごきませんでした"
        },
        "plain_form": {
            "present": "うごく",
            "present_negative": "うごかない",
            "past": "うごいた",
            "past_negative": "うごかなかった"
        },
        "te_form": {
            "present": "うごいて"
        }
    },
    {
        "id": "dlxG2yj8kMy-xRI",
        "chapter": 21,
        "english": "quit; give up",
        "kanji": "止めます",
        "verb_group": 2,
        "polite_form": {
            "present": "やめます",
            "present_negative": "やめません",
            "past": "やめました",
            "past_negative": "やめませんでした"
        },
        "plain_form": {
            "present": "やめる",
            "present_negative": "やめない",
            "past": "やめた",
            "past_negative": "やめなかった"
        },
        "te_form": {
            "present": "やめて"
        }
    },
    {
        "id": "vWdyc80ckLi",
        "chapter": 21,
        "english": "pay attention",
        "kanji": "点ける",
        "verb_group": 2,
        "polite_form": {
            "present": "さ を つけます",
            "present_negative": "さ を つけません",
            "past": "さ を つけました",
            "past_negative": "さ を つけませんでした"
        },
        "plain_form": {
            "present": "さ を つける",
            "present_negative": "さ を つけない",
            "past": "さ を つけた",
            "past_negative": "さ を つけなかった"
        },
        "te_form": {
            "present": "さ を つけて"
        }
    },
    {
        "id": "P-HjReJ1TvA-I",
        "chapter": 22,
        "english": "put on (upper body)",
        "kanji": "着ます",
        "verb_group": 2,
        "polite_form": {
            "present": "きます",
            "present_negative": "きません",
            "past": "きました",
            "past_negative": "きませんでした"
        },
        "plain_form": {
            "present": "きる",
            "present_negative": "きない",
            "past": "きた",
            "past_negative": "きなかった"
        },
        "te_form": {
            "present": "きて"
        }
    },
    {
        "id": "0Bh6OxYR9Wx",
        "chapter": 22,
        "english": "put on (lower body)",
        "kanji": "履きます",
        "verb_group": 1,
        "polite_form": {
            "present": "はきます",
            "present_negative": "はきません",
            "past": "はきました",
            "past_negative": "はきませんでした"
        },
        "plain_form": {
            "present": "はく",
            "present_negative": "はかない",
            "past": "はいた",
            "past_negative": "はかなかった"
        },
        "te_form": {
            "present": "はいて"
        }
    },
    {
        "id": "j6WkkLzIgQG",
        "chapter": 22,
        "english": "put on (head)",
        "kanji": "被ります",
        "verb_group": 1,
        "polite_form": {
            "present": "かぶきます",
            "present_negative": "かぶきません",
            "past": "かぶきました",
            "past_negative": "かぶきませんでした"
        },
        "plain_form": {
            "present": "かぶる",
            "present_negative": "かぶらない",
            "past": "かぶいた",
            "past_negative": "かぶらなかった"
        },
        "te_form": {
            "present": "かぶって"
        }
    },
    {
        "id": "Z5FMaXPfyzd-TYsq-6US",
        "chapter": 22,
        "english": "put on (glasses)",
        "kanji": "かけます",
        "verb_group": 2,
        "polite_form": {
            "present": "かけます",
            "present_negative": "かけません",
            "past": "かけました",
            "past_negative": "かけませんでした"
        },
        "plain_form": {
            "present": "かける",
            "present_negative": "かけない",
            "past": "かけた",
            "past_negative": "かけなかった"
        },
        "te_form": {
            "present": "かけて"
        }
    },
    {
        "id": "Cl1JR9lciGP",
        "chapter": 22,
        "english": "be born",
        "kanji": "生まれます",
        "verb_group": 2,
        "polite_form": {
            "present": "うまれます",
            "present_negative": "うまれません",
            "past": "うまれました",
            "past_negative": "うまれませんでした"
        },
        "plain_form": {
            "present": "うまれる",
            "present_negative": "うまれない",
            "past": "うまれた",
            "past_negative": "うまれなかった"
        },
        "te_form": {
            "present": "うまれて"
        }
    },
    {
        "id": "Md-RrZTjH8i",
        "chapter": 23,
        "english": "turn",
        "kanji": "回します",
        "verb_group": 1,
        "polite_form": {
            "present": "まわします",
            "present_negative": "まわしません",
            "past": "まわしました",
            "past_negative": "まわしませんでした"
        },
        "plain_form": {
            "present": "まわす",
            "present_negative": "まわちない",
            "past": "まわした",
            "past_negative": "まわちなかった"
        },
        "te_form": {
            "present": "まわして"
        }
    },
    {
        "id": "vgPo53_lNbH",
        "chapter": 23,
        "english": "pull",
        "kanji": "引きます",
        "verb_group": 1,
        "polite_form": {
            "present": "ひきます",
            "present_negative": "ひきません",
            "past": "ひきました",
            "past_negative": "ひきませんでした"
        },
        "plain_form": {
            "present": "ひく",
            "present_negative": "ひかない",
            "past": "ひいた",
            "past_negative": "ひかなかった"
        },
        "te_form": {
            "present": "ひいて"
        }
    },
    {
        "id": "Jl2frisJezQ",
        "chapter": 23,
        "english": "change",
        "kanji": "変えます",
        "verb_group": 2,
        "polite_form": {
            "present": "かえます",
            "present_negative": "かえません",
            "past": "かえました",
            "past_negative": "かえませんでした"
        },
        "plain_form": {
            "present": "かえる",
            "present_negative": "かえない",
            "past": "かえた",
            "past_negative": "かえなかった"
        },
        "te_form": {
            "present": "かえて"
        }
    },
    {
        "id": "oS2pISlnGwH",
        "chapter": 23,
        "english": "touch",
        "kanji": "触ります",
        "verb_group": 1,
        "polite_form": {
            "present": "さわります",
            "present_negative": "さわりません",
            "past": "さわりました",
            "past_negative": "さわりませんでした"
        },
        "plain_form": {
            "present": "さわる",
            "present_negative": "さわらない",
            "past": "さわった",
            "past_negative": "さわらなかった"
        },
        "te_form": {
            "present": "さわって"
        }
    },
    {
        "id": "t7qgAoaK7bS",
        "chapter": 23,
        "english": "walk",
        "kanji": "歩きます",
        "verb_group": 1,
        "polite_form": {
            "present": "あるきます",
            "present_negative": "あるきません",
            "past": "あるきました",
            "past_negative": "あるきませんでした"
        },
        "plain_form": {
            "present": "あるく",
            "present_negative": "あるかない",
            "past": "あるいた",
            "past_negative": "あるかなかった"
        },
        "te_form": {
            "present": "あるいて"
        }
    },
    {
        "id": "rtmOq0y2UB_",
        "chapter": 23,
        "english": "cross (object)",
        "kanji": "渡ります",
        "verb_group": 1,
        "polite_form": {
            "present": "わたります",
            "present_negative": "わたりません",
            "past": "わたりました",
            "past_negative": "わたりませんでした"
        },
        "plain_form": {
            "present": "わたる",
            "present_negative": "わたらない",
            "past": "わたった",
            "past_negative": "わたらなかった"
        },
        "te_form": {
            "present": "わたって"
        }
    },
    {
        "id": "RA6n2Hy5oCI",
        "chapter": 23,
        "english": "turn",
        "kanji": "ま借ります",
        "verb_group": 1,
        "polite_form": {
            "present": "まがります",
            "present_negative": "まがりません",
            "past": "まがりました",
            "past_negative": "まがりませんでした"
        },
        "plain_form": {
            "present": "まがる",
            "present_negative": "まがらない",
            "past": "まがった",
            "past_negative": "まがらなかった"
        },
        "te_form": {
            "present": "まがって"
        }
    },
    {
        "id": "sNzpUejd6ef",
        "chapter": 24,
        "english": "give (me)",
        "kanji": "呉れます",
        "verb_group": 2,
        "polite_form": {
            "present": "くれます",
            "present_negative": "くれません",
            "past": "くれました",
            "past_negative": "くれませんでした"
        },
        "plain_form": {
            "present": "くれる",
            "present_negative": "くれない",
            "past": "くれた",
            "past_negative": "くれなかった"
        },
        "te_form": {
            "present": "くれて"
        }
    },
    {
        "id": "CozbrGhbIE4",
        "chapter": 24,
        "english": "repair; correct",
        "kanji": "直しまし",
        "verb_group": 1,
        "polite_form": {
            "present": "なおします",
            "present_negative": "なおしません",
            "past": "なおしました",
            "past_negative": "なおしませんでした"
        },
        "plain_form": {
            "present": "なおす",
            "present_negative": "なおちない",
            "past": "なおした",
            "past_negative": "なおちなかった"
        },
        "te_form": {
            "present": "なおして"
        }
    },
    {
        "id": "O18fHZj5Jvp",
        "chapter": 24,
        "english": "take (person)",
        "kanji": "連れて 行きます",
        "verb_group": 1,
        "polite_form": {
            "present": "つれて いきます",
            "present_negative": "つれて いきません",
            "past": "つれて いきました",
            "past_negative": "つれて いきませんでした"
        },
        "plain_form": {
            "present": "つれて いく",
            "present_negative": "つれて いかない",
            "past": "つれて いいた",
            "past_negative": "つれて いかなかった"
        },
        "te_form": {
            "present": "つれて いいて"
        }
    },
    {
        "id": "yhPosz3evUF",
        "chapter": 24,
        "english": "bring (person)",
        "kanji": "連れて来ます",
        "verb_group": 3,
        "polite_form": {
            "present": "つれて きます",
            "present_negative": "つれて きません",
            "past": "つれて きました",
            "past_negative": "つれて きませんでした"
        },
        "plain_form": {
            "present": "つれて くる",
            "present_negative": "つれて こない",
            "past": "つれて きた",
            "past_negative": "つれて こなかった"
        },
        "te_form": {
            "present": "つれて いて"
        }
    },
    {
        "id": "sWvu7-blgxW",
        "chapter": 24,
        "english": "escort (person)",
        "kanji": "送ります",
        "verb_group": 1,
        "polite_form": {
            "present": "おくります",
            "present_negative": "おくりません",
            "past": "おくりました",
            "past_negative": "おくりませんでした"
        },
        "plain_form": {
            "present": "おくる",
            "present_negative": "おくらない",
            "past": "おくった",
            "past_negative": "おくらなかった"
        },
        "te_form": {
            "present": "おくって"
        }
    },
    {
        "id": "K0PM_X6fjg5",
        "chapter": 24,
        "english": "introduce",
        "kanji": "紹介します",
        "verb_group": 3,
        "polite_form": {
            "present": "しょうかいします",
            "present_negative": "しょうかいしません",
            "past": "しょうかいしました",
            "past_negative": "しょうかいしませんでした"
        },
        "plain_form": {
            "present": "しょうかいする",
            "present_negative": "しょうかいさない",
            "past": "しょうかいした",
            "past_negative": "しょうかいさなかった"
        },
        "te_form": {
            "present": "しょうかいして"
        }
    },
    {
        "id": "lpQKmCvLEPN",
        "chapter": 24,
        "english": "show around",
        "kanji": "案内します",
        "verb_group": 3,
        "polite_form": {
            "present": "あんないします",
            "present_negative": "あんないしません",
            "past": "あんないしました",
            "past_negative": "あんないしませんでした"
        },
        "plain_form": {
            "present": "あんないする",
            "present_negative": "あんないさない",
            "past": "あんないした",
            "past_negative": "あんなかったさない"
        },
        "te_form": {
            "present": "あんないして"
        }
    },
    {
        "id": "0cBmpx_Fax4-NFeSup",
        "chapter": 24,
        "english": "explain",
        "kanji": "説明します",
        "verb_group": 3,
        "polite_form": {
            "present": "せつめいします",
            "present_negative": "せつめいしません",
            "past": "せつめいしました",
            "past_negative": "せつめいしませんでした"
        },
        "plain_form": {
            "present": "せつめいする",
            "present_negative": "せつめいさない",
            "past": "せつめいした",
            "past_negative": "せつめいさなかった"
        },
        "te_form": {
            "present": "せつめいして"
        }
    },
    {
        "id": "k-qmsRWzv2a",
        "chapter": 25,
        "english": "think; consider",
        "kanji": "考えます",
        "verb_group": 2,
        "polite_form": {
            "present": "かんがえます",
            "present_negative": "かんがえません",
            "past": "かんがえました",
            "past_negative": "かんがえませんでした"
        },
        "plain_form": {
            "present": "かんがえる",
            "present_negative": "かんがえない",
            "past": "かんがえた",
            "past_negative": "かんがえなかった"
        },
        "te_form": {
            "present": "かんがえて"
        }
    },
    {
        "id": "tinq7z9aZNe",
        "chapter": 25,
        "english": "arrive",
        "kanji": "着きます",
        "verb_group": 1,
        "polite_form": {
            "present": "つきます",
            "present_negative": "つきません",
            "past": "つきました",
            "past_negative": "つきませんでした"
        },
        "plain_form": {
            "present": "つく",
            "present_negative": "つかない",
            "past": "ついた",
            "past_negative": "つかなかった"
        },
        "te_form": {
            "present": "ついて"
        }
    },
    {
        "id": "eJZHMAMo0Wv",
        "chapter": 25,
        "english": "grow old",
        "kanji": "年 を 取ります",
        "verb_group": 1,
        "polite_form": {
            "present": "とし を とります",
            "present_negative": "とし を とりません",
            "past": "とし を とりました",
            "past_negative": "とし を とりませんでした"
        },
        "plain_form": {
            "present": "とし を とる",
            "present_negative": "とし を とらない",
            "past": "とし を とった",
            "past_negative": "とし を とらなかった"
        },
        "te_form": {
            "present": "とし を とって"
        }
    },
    {
        "id": "K-11FNvSIpo",
        "chapter": 25,
        "english": "be enough",
        "kanji": "足ります",
        "verb_group": 2,
        "polite_form": {
            "present": "たります",
            "present_negative": "たりません",
            "past": "たりました",
            "past_negative": "たりませんでした"
        },
        "plain_form": {
            "present": "たりる",
            "present_negative": "たりない",
            "past": "たりた",
            "past_negative": "たりなかった"
        },
        "te_form": {
            "present": "たって"
        }
    },
    {
        "id": "yl8D1Aoyl",
        "chapter": 26,
        "english": "check, take a look at",
        "kanji": "見ます",
        "verb_group": 2,
        "polite_form": {
            "present": "みます",
            "present_negative": "みません",
            "past": "みました",
            "past_negative": "みませんでした"
        },
        "plain_form": {
            "present": "みる",
            "present_negative": "みない",
            "past": "みた",
            "past_negative": "みなかった"
        },
        "te_form": {
            "present": "みて"
        }
    },
    {
        "id": "h0PtThYOM",
        "chapter": 26,
        "english": "look for, search",
        "kanji": "探します",
        "verb_group": 1,
        "polite_form": {
            "present": "さがします",
            "present_negative": "さがしません",
            "past": "さがしました",
            "past_negative": "さがしませんでした"
        },
        "plain_form": {
            "present": "さがす",
            "present_negative": "さがさない",
            "past": "さがさなかった",
            "past_negative": "さがさくなかった"
        },
        "te_form": {
            "present": "さがして"
        }
    },
    {
        "id": "YHUCYhD83",
        "chapter": 26,
        "english": "be late",
        "kanji": "送れます",
        "verb_group": 2,
        "polite_form": {
            "present": "おくれます",
            "present_negative": "おくれません",
            "past": "",
            "past_negative": ""
        },
        "plain_form": {
            "present": "",
            "present_negative": "",
            "past": "",
            "past_negative": ""
        },
        "te_form": {
            "present": ""
        }
    },
    {
        "id": "nKSEM6o1q",
        "chapter": 26,
        "english": "be on time",
        "kanji": "間に合います",
        "verb_group": 2,
        "polite_form": {
            "present": "まにあいます",
            "present_negative": "まにあいました",
            "past": "まにあいました",
            "past_negative": "まにあいませんでした"
        },
        "plain_form": {
            "present": "まにあっている",
            "present_negative": "まにあわない",
            "past": "まにあうっていた",
            "past_negative": "まにあっていなかった"
        },
        "te_form": {
            "present": "まにあっていて"
        }
    },
    {
        "id": "ZOUamhZk8",
        "chapter": 26,
        "english": "to do",
        "kanji": "やる",
        "verb_group": 1,
        "polite_form": {
            "present": "やります",
            "present_negative": "やりません",
            "past": "やりました",
            "past_negative": "やりませんでした"
        },
        "plain_form": {
            "present": "やる",
            "present_negative": "やらない",
            "past": "やった",
            "past_negative": "やらなかった"
        },
        "te_form": {
            "present": "やって"
        }
    },
    {
        "id": "Rv7gfS6oq",
        "chapter": 26,
        "english": "pick up",
        "kanji": "拾います",
        "verb_group": 1,
        "polite_form": {
            "present": "ひろいます",
            "present_negative": "ひろいません",
            "past": "ひろいました",
            "past_negative": "ひろいませんでした"
        },
        "plain_form": {
            "present": "ひるう",
            "present_negative": "ひろわない",
            "past": "ひろった",
            "past_negative": "ひろなかった"
        },
        "te_form": {
            "present": "ひろって"
        }
    },
    {
        "id": "SVxrxK7SA",
        "chapter": 26,
        "english": "contact, get in touch with",
        "kanji": "連絡します",
        "verb_group": 1,
        "polite_form": {
            "present": "れんらくします",
            "present_negative": "れんらくしません",
            "past": "れんらくしました",
            "past_negative": "れんらくしませんでした"
        },
        "plain_form": {
            "present": "れんらくする",
            "present_negative": "れんらくしない",
            "past": "れんらくしった",
            "past_negative": "れんらくしなかった"
        },
        "te_form": {
            "present": "れんらくして"
        }
    },
    {
        "id": "oOa3wiMA7",
        "chapter": 26,
        "english": "tidy up, order",
        "kanji": "片付ける",
        "verb_group": 1,
        "polite_form": {
            "present": "かたづけます",
            "present_negative": "かたづけません",
            "past": "かたづけました",
            "past_negative": "かたづけませんでした"
        },
        "plain_form": {
            "present": "かたづける",
            "present_negative": "かたづかない",
            "past": "かたづけた",
            "past_negative": "かたづけなかった"
        },
        "te_form": {
            "present": "かたづけて"
        }
    },
    {
        "id": "YHUCYhD83",
        "chapter": 28,
        "english": "commute",
        "kanji": "通います",
        "verb_group": 2,
        "polite_form": {
            "present": "かよいます",
            "present_negative": "かよいません",
            "past": "かよいました",
            "past_negative": "かよいませんでした"
        },
        "plain_form": {
            "present": "かよう",
            "present_negative": "かよわない",
            "past": "かよった",
            "past_negative": "かわなかった"
        },
        "te_form": {
            "present": "かよって"
        }
    },
];
exports["default"] = verbs;
