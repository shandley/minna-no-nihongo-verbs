export interface Verb {
    id: string;
    chapter: number;
    english: string;
    kanji: string;
    verb_group: 1 | 2 | 3;
    polite_form: {
        present: string;
        present_negative: string;
        past: string;
        past_negative: string;
    };
    plain_form: {
        present: string;
        present_negative: string;
        past: string;
        past_negative: string;
    };
    te_form: {
        present: string;
    };
}
declare const verbs: Verb[];
export default verbs;
