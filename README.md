# minna-no-nihongo-verbs

A collection of verbs which appear in the Minna no Nihongo (みんなの日本語) second edition textbook, provided in JSON format.

This is provided as a resource to students creating study tools while using the textbook.

## installation

`npm install minna-no-nihongo-verbs`

## Usage

```javascript
import verbs from 'minna-no-nihongo-verbs'

const chapterFiveVerbs = verbs.filter(v => v.chapter === 5)

/**
[
  {
    "id": "9JcNZNMlgd",
    chapter: 5,
    "english": "go (location)",
    "kanji": "行きます",
    "verb_group": 1,
    "polite_form": {
      "present": "いきます",
      "present_negative": "いきません",
      "past": "いきました",
      "past_negative": "いきませんでした"
    },
    "plain_form": {
      "present": "いく",
      "present_negative": "いかない",
      "past": "いいた",
      "past_negative": "いかなかった"
    },
    "te_form": {
      "present": "いって"
    }
  },
  ...
]
 */
```
Each verb item is provided in following format:

```typescript
export interface Verb {
  id: string
  chapter: number
  english: string
  kanji: string
  verb_group: 1 | 2 | 3
  polite_form: {
    present: string
    present_negative: string
    past: string
    past_negative: string
  },
  plain_form: {
    present: string
    present_negative: string
    past: string
    past_negative: string
  }
  te_form: {
    present: string
  }
}
```

Id's are generated using [shortId](https://github.com/dylang/shortid). In cases where kanji does not exist or could not be determined (eg. ならいます) the hirigana is substituted.

## Exclusions

The following items were excluded as they provide a generic definition repeating a previous verb, but add an object

- のみます was excluded from chapter 16 as it appears in chapter 6
- かえます was excluded from chapter 18 as it has previous appearances
- お祭り あります was excluded from chapter 21
- りゅうがく します was excluded from chapter 21
- めがね を します was excluded from chapter 22
- ネクタイ を します was excluded from chapter 22
- 先生 に ききます (ask) was excluded from chapter 23
- でます (of change) was excluded from chapter 23

## Issues

Please report any missing or incorrect entries via [gitlab](https://gitlab.com/shandley/minna-no-nihongo-verbs/issues).

## Changelog

```
0.2.2 Add chapter 26 verbs

0.2.1 Correction to a verb (まがります)

0.2.0 Changed structure and included more variants

0.1.1 Found and changed a duplicate id

0.1.0 Looking pretty complete

0.0.12 Move to Gitlab, update docs

0.0.11 Added missing items from chapter 18 and 19

0.0.10 Corrections and kanji for remaining items

0.0.9 Add te forms for each verb

0.0.8 Add negative and dictionary forms for each verb

0.0.7 More corrections

0.0.6 Corrections for chapter 13

0.0.5 Bug with default export

0.0.4 Remove the use of enum for verb type

0.0.3 Add verb types and missing items

0.0.2 Properly format and remove a random hidden U+200B character

0.0.1 Initial publication
```